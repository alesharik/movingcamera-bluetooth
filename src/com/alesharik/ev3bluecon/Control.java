package com.alesharik.ev3bluecon;

import com.j4ev3.core.Brick;
import com.j4ev3.core.Motor;
import com.j4ev3.core.Sensor;
import com.j4ev3.desktop.BluetoothComm;

public class Control {
    private static final byte RIGHT_MOTOR = Motor.PORT_C;
    private static final byte LEFT_MOTOR = Motor.PORT_B;
    private static final byte STABIL_Z = Motor.PORT_D;
    private static final byte STABIL_Y = Motor.PORT_A;
    private static final byte SCANNER_FRONT = Sensor.PORT_4;
    private static final byte SCANNER_BACK = Sensor.PORT_1;
    private static final CameraMover mover = new CameraMover();
    private static Brick brick;
    private static final DistanceChecker distanceChecker = new DistanceChecker();

    public static void start() {
        brick = new Brick(new BluetoothComm("00165348d0bc"));
        try {
            Thread.sleep(1000);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        mover.start();
        distanceChecker.start();
        Runtime.getRuntime().addShutdownHook(new ShutdownThread());
    }

    public static void rotateLeftWheel(int speedPercent) {
        moveMotor(speedPercent, LEFT_MOTOR);
    }

    public static void rotateRightWheel(int speedPercent) {
        moveMotor(speedPercent, RIGHT_MOTOR);
    }

    private static void moveMotor(int speedPercent, byte rightMotor) {
        Motor motor = brick.getMotor();
        if(speedPercent < 5 && speedPercent > -5)
            motor.stopMotor(rightMotor, false);
        else if(speedPercent > 0) {
            motor.setPolarity(rightMotor, 1);
            motor.turnAtSpeed(rightMotor, speedPercent);
        } else if(speedPercent < 0) {
            motor.setPolarity(rightMotor, -1);
            motor.turnAtSpeed(rightMotor, -speedPercent);
        }
    }

    public static void brakeWheels() {
        brick.getMotor().stopMotor((byte) (RIGHT_MOTOR | LEFT_MOTOR), true);
    }

    public static void setFloating(boolean is) {
        System.err.println("FLOATING NOT IMPLEMENTED");
    }

    public static void moveCamera(CameraMoveType type) {
        mover.setMove(type);
    }

    public static boolean canMoveForward() {
        return distanceChecker.isCanMoveForward();
    }

    public static boolean canMoveBack() {
        return distanceChecker.isCanMoveBack();
    }

    private static final class ShutdownThread extends Thread {
        @Override
        public void run() {
            brick.getMotor().stopMotor(Motor.PORT_ALL, true);
            brick.disconnect();
        }
    }

    public enum CameraMoveType {
        NO,
        LEFT,
        RIGHT,
        UP,
        DOWN
    }

    private static final class CameraMover extends Thread {
        private static final int MOVE_TIME = 10;
        private volatile CameraMoveType move;
        private volatile CameraMoveType current;
        private volatile int ms = 0;

        @Override
        public void run() {
            Motor motor = brick.getMotor();
            while (true) {
                try {
                    if (ms > 0) {
                        ms--;
                        current = move;
                    } else
                        current = CameraMoveType.NO;
                    if (current == CameraMoveType.NO) {
                        motor.stopMotor((byte) (STABIL_Y | STABIL_Z), false);
                    } else if (current == CameraMoveType.LEFT) {
                        motor.setPolarity(STABIL_Z, 1);
                        motor.turnAtSpeed(STABIL_Z, 20);
                    } else if (current == CameraMoveType.RIGHT) {
                        motor.setPolarity(STABIL_Z, -1);
                        motor.turnAtSpeed(STABIL_Z, 20);
                    } else if (current == CameraMoveType.UP) {
                        motor.setPolarity(STABIL_Y, 1);
                        motor.turnAtSpeed(STABIL_Y, 20);
                    } else if (current == CameraMoveType.DOWN) {
                        motor.setPolarity(STABIL_Y, -1);
                        motor.turnAtSpeed(STABIL_Y, 20);
                    }
                    Thread.sleep(5);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        public void setMove(CameraMoveType type) {
            move = type;
            ms = MOVE_TIME;
        }
    }

    private static final class DistanceChecker extends Thread {
        private volatile boolean canMoveForward = true;
        private volatile boolean canMoveBack = true;

        public DistanceChecker() {
            setDaemon(true);
        }

        @Override
        public void run() {
            System.out.println("DistanceChecker started");
            while (isAlive() && !isInterrupted()) {
                float front = brick.getSensor().getValueSI(SCANNER_FRONT, Sensor.TYPE_ULTRASONIC, Sensor.ULTRASONIC_SI_CM);
                float back = brick.getSensor().getValueSI(SCANNER_BACK, Sensor.TYPE_ULTRASONIC, Sensor.ULTRASONIC_SI_CM);
                canMoveForward = !(front < 10);
                canMoveBack = !(back < 10);
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        }

        public boolean isCanMoveForward() {
            return canMoveForward;
        }

        public boolean isCanMoveBack() {
            return canMoveBack;
        }
    }
}
