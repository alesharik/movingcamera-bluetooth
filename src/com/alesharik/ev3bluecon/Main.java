package com.alesharik.ev3bluecon;

import java.io.DataInputStream;
import java.io.IOException;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.nio.charset.StandardCharsets;

public class Main {
    private static final byte TYPE_STABIL_Z = 1;
    private static final byte TYPE_STABIL_Y = 2;
    private static final byte TYPE_STABIL_Z_BACK = 21;
    private static final byte TYPE_STABIL_Y_BACK = 22;
    private static final byte TYPE_WHEEL = 3;
    private static final byte TYPE_WHEEL_RIGHT = 4;
    private static final byte TYPE_TURN_LEFT = 23;
    private static final byte TYPE_TURN_RIGHT = 24;
    private static final byte TYPE_WHEEL_BRAKE = 5;
    private static final byte TYPE_WHEEL_FLOAT = 6;
    private static final byte TYPE_WHEEL_MANUAL = 123;
    private static final byte TYPE_SPEAK_COMMAND = 124;

    public static void main(String[] args) throws IOException {
        Control.start();
        ServerSocket serverSocket = new ServerSocket(1200, 0, InetAddress.getByName("0.0.0.0"));
        System.out.println("Started");
        while (!Thread.currentThread().isInterrupted()) {
            Socket s = serverSocket.accept();
            new Handler(s).start();
        }
    }

    private static final class Handler extends Thread {
        private final Socket socket;

        public Handler(Socket socket) {
            this.socket = socket;
        }

        @Override
        public void run() {
            System.out.println("Socket open");
            try {
                DataInputStream stream = new DataInputStream(socket.getInputStream());
                while (!socket.isClosed()) {
                    byte type = stream.readByte();
                    switch (type) {
                        case TYPE_STABIL_Y:
                            Control.moveCamera(Control.CameraMoveType.RIGHT);
                            break;
                        case TYPE_STABIL_Y_BACK:
                            Control.moveCamera(Control.CameraMoveType.LEFT);
                            break;
                        case TYPE_STABIL_Z:
                            Control.moveCamera(Control.CameraMoveType.UP);
                            break;
                        case TYPE_STABIL_Z_BACK:
                            Control.moveCamera(Control.CameraMoveType.DOWN);
                            break;
                        case TYPE_WHEEL:
                            byte speedPercent = stream.readByte();
                            Control.rotateLeftWheel(speedPercent);
                            Control.rotateRightWheel(speedPercent);
                            break;
                        case TYPE_WHEEL_RIGHT:
                            break;
                        case TYPE_WHEEL_BRAKE:
                            Control.brakeWheels();
                            break;
                        case TYPE_WHEEL_FLOAT:
                            Control.setFloating(stream.readBoolean());
                            break;
                        case TYPE_TURN_LEFT:
                            int spd = stream.readByte();
                            Control.rotateRightWheel(spd + 10);
                            Control.rotateLeftWheel(spd - 10);
                            break;
                        case TYPE_TURN_RIGHT:
                            int s = stream.readByte();
                            Control.rotateRightWheel(s - 10);
                            Control.rotateLeftWheel(s + 10);
                            break;
                        case TYPE_WHEEL_MANUAL:
                            byte right = stream.readByte();
                            byte left = stream.readByte();
                            if (Math.abs(right) > 5 && Math.abs(left) > 5) {
                                if ((Math.abs(right - left) < 5)) {
                                    if (right < 0 && left < 0) {
                                        if (Control.canMoveBack()) {
                                            Control.rotateLeftWheel(left);
                                            Control.rotateRightWheel(right);
                                        }
                                    }
                                    if (right > 0 && left > 0) {
                                        if (Control.canMoveForward()) {
                                            Control.rotateLeftWheel(left);
                                            Control.rotateRightWheel(right);
                                        }
                                    }
                                } else {
                                    Control.rotateLeftWheel(left);
                                    Control.rotateRightWheel(right);
                                }
                            } else {
                                Control.rotateLeftWheel(0);
                                Control.rotateRightWheel(0);
                            }
                            break;
                        case TYPE_SPEAK_COMMAND:
                            System.err.println("NOT SUPPORTED");
                            break;
                        default:
                            System.out.println("Unknown command type " + type);
                    }
                    socket.getOutputStream().write(1);
                    Thread.sleep(25);
                }
                socket.close();
            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }
    }
}
