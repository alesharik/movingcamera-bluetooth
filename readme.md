# MovingCamera bluetooth controller
This app controls robot by using bluetooth

| Port     | Target                | Description                        |
|----------|-----------------------|------------------------------------|
| Motor.C  | Right move motor      |                                    |
| Motor.B  | Left move motor       |                                    |
| Motor.D  | Z axis stabilizator   | Motor that rotate camera by Z axis |
| Motor.A  | Y axis stabilizator   | Motor that rotate camera by Y axis |
| Sensor.4 | Front distance sensor |                                    |
| Sensor.1 | Back distance sensor |                                    |